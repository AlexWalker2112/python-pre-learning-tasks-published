def factors(number):
    # ==============
    factors=[]
    for i in range(1,number+1):
        if number%i==0:
            factors.append(i)
    factors.remove(1)
    factors.remove(number)

    if not factors:
        
        number = str(number)
        number = "“" + number + " is a prime number”"
        return number  
        
    else:
        return factors
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
