def vowel_swapper(string):
    # ==============
    # Your code here
    
    
    stringlowercase = string.lower()
    
    ######
    
    def nth_repl(string, sub, repl, n, stringlowercase, subCap, replCap):
        find = stringlowercase.find(sub) #Find is the replacement value is in the string
        
        i = find != -1 #If -1 then the replacement is not in the string
        
        while find != -1 and i != n:  # loop util we find the nth or we find no match
            
        
            find = stringlowercase.find(sub, find + 1)# find + 1 means we start searching from after the last match
            if find == -1:
                i = i
            else:
                i += 1
        
        
        if i == n: # If i is equal to n we found nth match so replace
            
            if string[find:find+1] != sub: #finds if character is capital or not
            
                return string[:find] + replCap + string[find+len(sub):]
            
            else:
            
                return string[:find] + repl + string[find+len(sub):]
        
        return string
    
    ######

    string = nth_repl(string,"a","4",2, stringlowercase, "A", "4")
    stringlowercase = string.lower()
        
    string = nth_repl(string,"e","3",2, stringlowercase,"e","3")
    stringlowercase = string.lower()
        
    string = nth_repl(string,"i","!",2, stringlowercase,"i","!")
    stringlowercase = string.lower()
        
    string = nth_repl(string,"u","|_|",2, stringlowercase,"u","|_|")
    stringlowercase = string.lower()
    
    string = nth_repl(string,"o","ooo",2, stringlowercase, "O", "000")
    stringlowercase = string.lower()
        
    return string

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
