def calculator(a, b, operator):
    # ==============
    astring = str(a)
    bstring = str(b)
    equation = astring + operator + bstring
    output = eval (equation)
    
    outputcheckint = int (output)
    outputcheckresult = output / outputcheckint
    
    if outputcheckresult == 1:
         #for int or float
        output = int (output)
        return output
    else:
        
        return output

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
